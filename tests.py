import unittest

from models import Teacher, Student, Quiz, Question
from quiz_exceptions import QuizTeacherAlreadyExist, StudentNotInTeacherClass, UnexpectedAnswer


class TestQuiz(unittest.TestCase):
    def setUp(self):
        self.teacher_1 = Teacher("Teacher 1")
        self.teacher_2 = Teacher("Teacher 2")

        self.student_1 = Student("Emre Soner Demir", 1)
        self.student_2 = Student("Nazli Ayse Demir", 2)
        self.student_3 = Student("Ahmet Musab Demir", 3)

        self.teacher_1.attach_students([self.student_1, self.student_2])
        self.teacher_2.attach_students([self.student_3])

        self.quiz_1 = Quiz("Quiz 1")
        self.quiz_2 = Quiz("Quiz 2")

        self.question_1 = Question("Question 1", 40, ['Yes', 'No'], 'Yes', self.quiz_1)
        self.question_2 = Question("Question 2", 50, ['Yes', 'No'], 'No', self.quiz_1)
        self.question_3 = Question("Question 3", 10, ['Yes', 'No'], 'Yes', self.quiz_1)
        self.question_4 = Question("Question 4", 40, ['Yes', 'No'], 'No', self.quiz_2)
        self.question_5 = Question("Question 5", 50, ['Yes', 'No'], 'Yes', self.quiz_2)
        self.question_6 = Question("Question 6", 10, ['Yes', 'No'], 'No', self.quiz_2)

        self.teacher_1.apply_quiz(self.quiz_1)
        self.teacher_2.apply_quiz(self.quiz_2)

    def test_total_grade(self):
        self.question_1.give_answer(self.student_1, 'Yes')  # 40 grade
        self.question_2.give_answer(self.student_1, 'Yes')  # 0 grade
        self.question_3.give_answer(self.student_1, 'Yes')  # 10 grade -> Total: 50

        self.question_1.give_answer(self.student_2, 'Yes')  # 40 grade
        self.question_2.give_answer(self.student_2, 'Yes')  # 0 grade -> Total: 40

        self.question_5.give_answer(self.student_3, 'No')  # 0 grade
        self.question_6.give_answer(self.student_3, 'Yes')  # 0 grade -> Total: 0

        self.assertEqual(50, self.teacher_1.total_grade(self.student_1))
        self.assertEqual(40, self.teacher_1.total_grade(self.student_2))
        self.assertEqual(0, self.teacher_2.total_grade(self.student_3))

    def test_exceptions(self):
        with self.assertRaises(QuizTeacherAlreadyExist):
            self.teacher_1.apply_quiz(self.quiz_2)

        with self.assertRaises(StudentNotInTeacherClass):
            self.question_5.give_answer(self.student_1, 'No')  # Question 5 and Quiz 2 in Teacher 2 class

        with self.assertRaises(UnexpectedAnswer):
            self.question_5.give_answer(self.student_3, 'Ok')  # Ok not in answers


if __name__ == '__main__':
    unittest.main()
