class QuizModelBaseException(Exception):
    def __init__(self, params=None):
        params = params or {}
        super(QuizModelBaseException, self).__init__(self.message.format(**params))


class QuizTeacherAlreadyExist(QuizModelBaseException):
    message = "Another teacher: {teacher} has already applied this quiz."


class StudentNotInTeacherClass(QuizModelBaseException):
    message = "This student: {student} not in this teacher: {teacher} class."


class UnexpectedAnswer(QuizModelBaseException):
    message = "This answer not in question answer choices. Choices are: {choices}."
