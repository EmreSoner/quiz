from collections import defaultdict

from quiz_exceptions import QuizTeacherAlreadyExist, StudentNotInTeacherClass, UnexpectedAnswer


class Teacher(object):
    def __init__(self, name):
        self.name = name
        self._students, self._quiz_list = [], []

    def attach_students(self, students):
        self._students.extend(students)

    def get_students(self):
        return self._students

    def apply_quiz(self, quiz):
        quiz.set_teacher(self)
        self._quiz_list.append(quiz)

    def get_quiz_list(self):
        return self._quiz_list

    def total_grade(self, student):
        return sum([x.get_grade(student) for x in self.get_quiz_list()])


class Student(object):
    def __init__(self, name, number):
        self.name, self.number = name, number


class Quiz(object):
    def __init__(self, name):
        self.name = name
        self._leader_board = defaultdict(int)
        self._teacher = None

    def set_grade(self, student, grade):
        self._leader_board[student.number] += grade

    def get_grade(self, student):
        return self._leader_board.get(student.number) or 0

    def set_teacher(self, teacher):
        if self.get_teacher():
            raise QuizTeacherAlreadyExist(params={'teacher': teacher.name})
        self._teacher = teacher

    def get_teacher(self):
        return self._teacher


class Question(object):
    def __init__(self, question, grade, answers, answer, quiz):
        self.question = question
        self.answers = answers or []
        self.answer = answer
        self.grade = grade
        self.quiz = quiz

    def give_answer(self, student, given_answer):
        teacher = self.quiz.get_teacher()
        if teacher and student not in teacher.get_students():
            raise StudentNotInTeacherClass(params={'student': student.name, 'teacher': teacher.name})

        if given_answer not in self.answers:
            raise UnexpectedAnswer(params={'choices': ', '.join(self.answers)})

        if self.answer != given_answer:
            return
        self.quiz.set_grade(student, self.grade)



